Vmod-variable README
See the end of file for copying conditions.

* Introduction

This file contains brief information about configuring, testing
and using vmod-variable. It is *not* intended as a replacement
for the documentation, and is provided as a brief reference only.
For accessing complete vmod-variable documentation, see the section
'Documentation' below.

For a list of differences between this module and vmod_var, see
the section "vmod_variable vs. vmod_var".

* Overview

This module provides enhanced variable support for VCL scripts.
It compiles for Varnish versions 6.0.0 - 6.4.0.

There are two kinds of variables: session-specific, which have the
lifespan of one HTTP session (connection) and cease to exist when
it is closed, and global, which are shared between all sessions.

Session-specific variables are typed, a pair of functions is provided
for setting and retrieveng variables od a particular type.  For
example the function set_duration sets a duration variable, and
get_duration retrieves its value:

   set beresp.ttl = variable.get_int("ttl");

Special functions are provided for testing if a variable is defined:
the defined() function returns true if the variable is defined, and
the type_of() function returns the type of the variable.

A special feature of this module are regset() and queryset()
functions.  The regset() function allows you to parse a string
(e.g. a URL or header value) according to a regular expression
and to set several variables at once to selected substrings of
the input string, optionally converting between different types.
For example, the fragment below sets ttl and grace parameters of
the object according to the Surrogate-Control header:

  variable.regset("ttl:d=\1s,grace:d=\2s",
                  "^(?:.*,)?max-age=([0-9]+)(?:+([0-9]+))",
                   beresp.http.Surrogate-Control);
  set beresp.ttl = variable.get_duration("ttl");
  set beresp.grace = variable.get_duration("grace");

The first argument to regset declares a list of variables to be
set, along with their types (":d" standing for "duration") and
values to be assigned (after = signs).  The latter use backreferenses
to refer to the substrings captured by the regular expression and add
the "s" suffix to each captured string to obtain a valid duration.

To obtain the same effect without variable.regset, one would have to
use regsub twice, casting the result explicitly

  set beresp.ttl = std.duration(regsub(beresp.http.Surrogate-Control,
                                     "^(?:.*,)?max-age=([0-9]+)(?:+([0-9]+))",
			             "\1s"));
  set beresp.grace = std.duration(regsub(beresp.http.Surrogate-Control,
                                     "^(?:.*,)?max-age=([0-9]+)(?:+([0-9]+))",
			             "\2s"));				     

The queryset() function treats its argument as a HTTP query, and
defines variables according to it.  For example:

  variable.queryset("q:s,n:i,p:i", regsub(req.url, ".*\?(.+)", "\1"));

If req.url contained "q=term&n=10&p=5", this call will define three
variables: a string variable "q" having the value "term", and two
integer variables "n" and "p" with the values 10 and 5
correspondingly.

* Functions

** Typed accessors:

VOID set(STRING name, STRING value)
VOID set_string(STRING name, STRING name)
	Define a string variable
	
STRING get(STRING name)
STRING get_string(STRING name)
	Retrieve the value of a string variable
	
VOID set_int(STRING name, INT value)
	Define an integer variable
INT get_int(STRING name)
	Retrieve the value of an integer variable
	
VOID set_real(STRING name, REAL value)
	Define a floating-point variable
REAL get_real(STRING name)	
	Retrieve the value of a floating-point variable
	
VOID set_duration(STRING name, DURATION value)
	Define a duration variable
DURATION get_duration(STRING name)
	Retrieve the value of a duration variable

VOID unset(STRING name)
	Unset the variable

VOID clear()
	Unset all variables

** Testing for existence

BOOL defined(STRING name)
	Test if the variable is defined
	
STRING type_of(STRING name)
	Return type of the variable (one of: UNSET, STRING, INT,
	REAL, DURATION)

** Special functions

VOID regset(STRING vardcl, STRING regex, STRING input)
	If input matches regular expression regex, initializes variables
        declared  in  vardcl to parts of input captured by parenthesized
        groups in regex.

	The parameter vardcl contains one or more comma-separated decla-
        rations, each one having the syntax NAME[:TYPE][=VALUE] (brackets
	denoting optional parts).  The :TYPE suffix declares the type
	of the variable (STRING, INT, REAL, DURATION). The =VALUE part
	defines its value.  It can refer to the nine captured
        substrings via the usual notation \1 through \9.

VOID queryset(STRING vardcl, STRING input)
        Parses input as HTTP query string and assigns variables according
	to vardcl.

	If vardcl is empty,  the  function  will  define  all  variables
        present  in  input  to string values.  Otherwies, only the named
        variables will be set.

	The value of the vardcl variable is a  comma-separated  list  of
        variable  names,  optionally  followed by :TYPE construct, whose
        meaning is the same as in regset.
	
** Global variables
	
VOID global_set(STRING name, STRING value)
	Set a global variable
STRING global_get(STRING name)
	Retrieve the value of a global variable
BOOL global_defined(STRING name)
	Test whether the global variable is defined
VOID global_unset(STRING name)
	Unset a global variable
VOID global_clear()
	Unset all global variables
	
* vmod_variable vs. vmod_var

This module is backward-compatibile with vmod_var.  To facilitate
transition, it uses the same naming scheme, so switching to use
vmod_variable in your VCL script is just a matter of replacing
s/var\./variable./

Main differences of vmod_variable from vmod_var:

1. Both Varnish 3 and 4 are supported;

2. Variables are stored in hash tables with open addressing, to
speed up accesses (vmod_var keeps them in singly-linked lists).

3. Functions for testing existence and types of variables, and for
unsetting a variable: defined(), type_of() and unset()

4. The regset() and queryset() functions; 

5. Additional functions for global variables: global_clear(),
global_defined() and global_unset().

* Installation

In order to compile the package you need to have the varnishd and
varnishapi packages installed.

Supposing that condition is met, run:

  ./configure

It should be able to automatically find the necessary components. In case 
it doesn't, tweak the configuration variables as necessary. The most 
important one is PKG_CONFIG_PATH, which contains a path (in the UNIX sense)
where the .pc files are located. It should contain a directory where the
'varnishapi.pc' file lives. Example usage:

  ./configure PKG_CONFIG_PATH=/usr/local/varnish/lib/pkgconfig:$PKG_CONFIG_PATH

Please read the file INSTALL for a detailed discussion of available variables
and command line options.

Once configured, do

  make

This will build the module.  After this step you can optionally run
'make test' to test the package.

Finally, run the following command as root:

  make install
  
* Documentation

The manual page vmod-variable(3) will be available after a successful
install.  To read it without actually installing the module, run
`man src/vmod-variable.3'.

An online copy of the documentation is available from
http://www.gnu.org.ua/software/vmod-variable.

* Bug reporting

Send bug reports and suggestions to <gray@gnu.org>

* Copyright information:

Copyright (C) 2013-2018 Sergey Poznyakoff

   Permission is granted to anyone to make or distribute verbatim copies
   of this document as received, in any medium, provided that the
   copyright notice and this permission notice are preserved,
   thus giving the recipient permission to redistribute in turn.

   Permission is granted to distribute modified versions
   of this document, or of portions of it,
   under the above conditions, provided also that they
   carry prominent notices stating who last changed them.

Local Variables:
mode: outline
paragraph-separate: "[ 	]*$"
version-control: never
End:
  
