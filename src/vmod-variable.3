.\" This file is part of Vmod-variable -*- nroff -*-
.\" Copyright (C) 2015-2022 Sergey Poznyakoff
.\"
.\" Vmod-variable is free software; you can redistribute it and/or modify
.\" it under the terms of the GNU General Public License as published by
.\" the Free Software Foundation; either version 3, or (at your option)
.\" any later version.
.\"
.\" Vmod-variable is distributed in the hope that it will be useful,
.\" but WITHOUT ANY WARRANTY; without even the implied warranty of
.\" MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.\" GNU General Public License for more details.
.\"
.\" You should have received a copy of the GNU General Public License
.\" along with vmod-variable.  If not, see <http://www.gnu.org/licenses/>.
.TH VMOD-VARIABLE 3 "August 21, 2022" "VMOD-VARIABLE" "User Reference"
.SH NAME
vmod-variable \- variable support for Varnish Cache
.SH SYNOPSIS

\fBimport variable\fR [\fIPATH\fR]\fB;\fR

.SH DESCRIPTION
This module provides enhanced variable support for \fBVCL\fR scripts.
.PP
There are two kind of variables: \fIsession-specific\fR and \fIglobal\fR.
Session-specific variables have a lifespan of one HTTP session and cease to
exist when it is closed.  Such variables can be of almost any VCL type:
.BR STRING ,
.BR INT ,
.BR REAL ", or"
.BR DURATION .
For each of these types a pair of functions is provided for setting the
variable and obtaining its value.
.PP
Two functions are provided for setting several variables at once.  The
\fBregset\fR function parses its input according to a regular expression
and sets variables to the captured strings, using sophisticated declaration
patterns.
.PP
The \fBqueryset\fR function parses its input as a HTTP query string and
sets the variables defined in it.
.PP
The \fIglobal\fR variables are shared between all threads and sessions.  They
can hold only string values.
.SH VMOD_VARIABLE VS. VMOD_VAR
This module doesn't share any code with \fBvmod_var\fR.  Compared with
the latter, it provides the following new features:
.PP
.nr step 1 1
.IP \n[step].
Both Varnish 3 and 4 are supported;
.IP \n+[step].
Variables are stored in hash tables with open addressing, to
speed up accesses (\fBvmod_var\fR keeps them in singly-linked lists).
.IP \n+[step].
Functions for testing existence and types of variables, and for
unsetting a variable:
.BR defined() ,
.BR type_of() ", and"
.BR unset() .
.IP \n+[step].
The \fBregset()\fR and \fBqueryset()\fR functions.
.IP \n+[step].
Additional functions for global variables:
.BR global_clear() ,
.BR global_defined() ", and"
.BR global_unset() .
.PP
To facilitate transition, \fBvmod_variable\fR uses the same naming
scheme as \fBvmod_var\fR, so switching to using it in your VCL script
is as simple as replacing \fBs/var\\./variable./\fR.
.SH FUNCTIONS
The function below operate on session-local variables.
.PP
.TP
\fBVOID set(STRING \fIname\fB, STRING \fIvalue\fB)\fR
Assigns \fIvalue\fR to the string variable \fIname\fR.
.TP
\fBSTRING get(STRING \fIname\fB)\fR
Returns value of the string variable \fIname\fR.  If the variable is not
defined, \fBNULL\fR is returned.
.TP
\fBVOID set_int(STRING \fIname\fB, INT \fIvalue\fB)\fR
Assigns \fIvalue\fR to the integer variable \fIname\fR.
.TP
\fBINT get_int(STRING \fIname\fB)\fR
Returns value of the integer variable \fIname\fR.  If the variable is not
defined, \fB0\fR is returned.
.TP
\fBVOID set_string(STRING \fIname\fB, STRING \fIvalue\fB)\fR
Same as \fBset\fR.
.TP
\fBSTRING get_string(STRING \fIname\fB)\fR
Same as \fBget\fR.
.TP
\fBVOID set_real(STRING \fIname\fB, REAL \fIvalue\fB)\fR
Assigns \fIvalue\fR to the floating-point variable \fIname\fR.
.TP
\fBREAL get_real(STRING \fIname\fB)\fR
Returns value of the floating-point variable \fIname\fR.  If the variable is
not defined, \fB0.000\fR is returned.
.TP
\fBVOID set_duration(STRING \fIname\fB, DURATION \fIvalue\fB)\fR
Assigns \fIvalue\fR to the duration variable \fIname\fR.
.TP
\fBDURATION get_duration(STRING \fIname\fB)\fR
Returns value of the duration variable \fIname\fR.  If the variable is not
defined, \fB0.000\fR is returned.
.TP
\fBVOID regset(STRING \fIvardcl\fB, STRING \fIregex\fB, STRING \fIinput\fB)\fR
If \fIinput\fR matches regular expression \fIregex\fR, initializes variables
declared in \fIvardcl\fR to parts of \fIinput\fR captured by parenthesized
groups in \fIregex\fR.

The parameter \fIvardcl\fR contains one or more comma-separated declarations.
Each declaration is a variable name, optionally followed by the following
constructs, in that order: \fB:\fITYPE\fR and \fB=\fIREPL\fR.

The \fB:\fITYPE\fR construct declares the type of the variable.  \fITYPE\fR
can be one of
.BR STRING ,
.BR INT ,
.BR REAL ,
.BR DURATION ,
or any abbreviation of these (case-insensitive).  E.g. \fBx:r\fR means
that the variable \fBx\fR is of \fBREAL\fR type.  If this part is omitted,
\fBSTRING\fR type is assumed.

The \fB=\fIREPL\fR suffix instructs the function to set the variable to the
value computed according to \fIREPL\fR, which is a replacement expression in
the sense of \fBregsub\fR.

If this part is omitted, the variable is initialized to the value of the
\fBN\fRth capture group from \fIregex\fR, where \fBN\fR is the position
of that declaration in \fIvardcl\fR.

Both parts can be used together, for example: \fBtime:d=\\1m\fR will set
the \fBtime\fR variable to the duration obtained by concatenating first
captured group with the suffix \fBm\fR.

Backreferences referring to unexisting capturing groups are retained
verbatim.
.TP
\fBVOID queryset(STRING \fIvardcl\fB, STRING \fIinput\fB)\fR
Parses \fIinput\fR as HTTP query string and assigns variables according
to \fIvardcl\fR.

If \fIvardcl\fR is empty, the function will define all variables present
in \fIinput\fR to string values.  Otherwies, only the named variables
will be set.

The value of the \fIvardcl\fR variable is a comma-separated list of variable
names, optionally followed by \fB:\fITYPE\fR construct, whose meaning is
the same as in \fBregset\fR.
.TP
.B VOID clear()
Re-initializes session-local variable table.  All variables are forgotten.
.PP
.TP
\fBBOOL defined(STRING \fIname\fB)\fR
Returns \fBTRUE\fR if the variable \fIname\fR is defined.
.TP
\fBSTRING type_of(STRING \fIname\fB)\fR
Returns type of the variable \fIname\fR.  Available types are:
.BR UNSET ,
.BR STRING ,
.BR INT ,
.BR REAL ,
.BR DURATION .
.TP
\fBVOID unset(STRING \fIname\fB)\fR
Unsets the variable \fIname\fR.
.SS Global variables
.PP
The functions below operate on global variables.  Global variables can
be hold only string values.
.PP
.TP
\fBVOID global_set(STRING \fIname\fB, STRING \fIvalue\fB)\fR
Assigns \fIvalue\fR to the global variable \fIname\fR.
.TP
\fBSTRING global_get(STRING \fIname\fB)\fR
Returns value of the global variable \fIname\fR.  If the variable is not
defined, \fBNULL\fR is returned.
.TP
\fBBOOL global_defined(STRING \fIname\fB)\fR
Returns \fBTRUE\fR if the global variable \fIname\fR is defined.
.TP
\fBVOID global_unset(STRING \fIname\fB)\fR
Unsets global variable \fIname\fR.
.TP
.B VOID global_clear()
Re-initializes global variable table.  All variables are forgotten.
.SH EXAMPLES
.nr step 1 1
.TP
.IP \n[step].
Set ttl and grace parameters of the object according to the
\fBSurrogate\-Control\fR header.
.PP
.EX
variable.regset("ttl:d=\\1s,grace:d=\\2s",
                "^(?:.*,)?\s*max\-age=([0\-9]+)(?:\+([0\-9]+))",
                beresp.http.Surrogate\-Control);
set beresp.ttl = variable.get_duration("ttl");
set beresp.grace = variable.get_duration("grace");
.EE
.TP
.IP \n+[step].
Set local variables from GET parameters.
.PP
.EX
if (req.request == "GET") {
  variable.queryset("", regsub(req.url, ".*\\?(.+)", "\\1"));
}
.EE
.\"
.\" The MANCGI variable is set by man.cgi script on Ulysses.
.\" The download.inc file contains the default DOWNLOAD section
.\" for man-based doc pages.
.if "\V[MANCGI]"WEBDOC" \{\
.	ds package vmod-variable
.	ds version 1.5
.	so download.inc
\}
.SH "SEE ALSO"
.BR vcl (7),
.BR varnishd (1).
.SH AUTHORS
Sergey Poznyakoff
.SH "BUG REPORTS"
Report bugs to <gray@gnu.org>.
.SH COPYRIGHT
Copyright \(co 2015-2022 Sergey Poznyakoff
.br
.na
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
.br
.ad
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
.\" Local variables:
.\" eval: (add-hook 'write-file-hooks 'time-stamp)
.\" time-stamp-start: ".TH [A-Z_][A-Z0-9_.-]* [0-9] \""
.\" time-stamp-format: "%:B %:d, %:y"
.\" time-stamp-end: "\""
.\" time-stamp-line-limit: 20
.\" end:
