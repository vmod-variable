# This file is part of vmod-variable
# Copyright (C) 2015-2020 Sergey Poznyakoff
#
# Vmod-variable is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# Vmod-variable is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with vmod-variable.  If not, see <http://www.gnu.org/licenses/>.

$Module variable 3 "Enhanced Variable Support for Varnish"

COLOPHON
========
Due to limitations of the vmodtool.py utility, this document contains
only prototypes of the functions provided by the **vmod-variable** module.
For a detailed documentation, please see vmod-variable(3) manual page.

$Function VOID set(PRIV_TASK, STRING, STRING)

$Function STRING get(PRIV_TASK, STRING)

$Function VOID global_set(STRING, STRING)

$Function STRING global_get(STRING)

$Function VOID set_int(PRIV_TASK, STRING, INT)

$Function INT get_int(PRIV_TASK, STRING)

$Function VOID set_string(PRIV_TASK, STRING, STRING)

$Function STRING get_string(PRIV_TASK, STRING)

$Function VOID set_real(PRIV_TASK, STRING, REAL)

$Function REAL get_real(PRIV_TASK, STRING)

$Function VOID set_duration(PRIV_TASK, STRING, DURATION)

$Function DURATION get_duration(PRIV_TASK, STRING)

$Function VOID clear(PRIV_TASK)

$Function VOID global_clear()

$Function BOOL global_defined(STRING)

$Function VOID global_unset(STRING)

$Function BOOL defined(PRIV_TASK, STRING)

$Function STRING type_of(PRIV_TASK, STRING)

$Function VOID unset(PRIV_TASK, STRING)

$Function VOID regset(PRIV_TASK, STRING, STRING, STRING)

$Function VOID queryset(PRIV_TASK, STRING, STRING)
